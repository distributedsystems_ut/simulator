from socket import AF_INET, SOCK_STREAM, socket
import random


def randomCoordinate(): return 360 * random.uniform(0, 1)
def randomCoordinates(): return str(randomCoordinate()) + "," + str(randomCoordinate())


class Mobile():
	def __init__(self, distributor, buffer_size, encoding):
		self.distributor = distributor
		self.buffer_size = buffer_size
		self.encoding = encoding

	def run(self):
		try:

			##### DISTRIBUTOR #####
			#######################
			self.s = socket(AF_INET, SOCK_STREAM)
			self.s.connect(self.distributor)

			# SENDING MESSAGE
			message = ("1/qwerty|b:ua@" + randomCoordinates()).encode(self.encoding)
			if self.s.sendall(message) != None: raise Exception("Unable to send: " + message)
			else: print("Sent: ", message)

			# RECEIVING RESPONSE
			response = self.s.recv(self.buffer_size)
			print("Received: ", response)

			# PARSING RESPONSE
			b_or_c = response.split(":")[-1].split("@")
			b_or_c_address = (b_or_c[0], int(b_or_c[1]))

			# DISCONNECTING
			self.s.close()
			#######################


			###### BALANCER/COMPUTATIONAL NODE #####
			########################################
			self.s = socket(AF_INET, SOCK_STREAM)
			self.s.connect(b_or_c_address)

			# SENDING 'NEW USER' MESSAGE
			message = "1/qwerty|n:010".encode(self.encoding)
			if self.s.sendall(message) != None: raise Exception("Unable to send: " + message)
			else: print("Sent: ", message)

			# RECEIVING RESPONSE
			response = self.s.recv(self.buffer_size)
			print("Received: ", response)

			# PARSING RESPONSE
			user_id = response.split(":")[-1]

			# SENDING 'LOCATION UPDATE' messages
			while True:
				message = ("1/qwerty|n:" + user_id + "@" + randomCoordinates()).encode(self.encoding)
				if self.s.sendall(message) != None: raise Exception("Unable to send: " + message)
				else: print("Sent: ", message)

				# RECEIVING RESPONSE
				response = self.s.recv(self.buffer_size)
				print("Received: ", response)

				# PARSING RESPONSE
				matches = response.split(":")[-1].split(",")

				# SENDING 'IP REQUEST' messages
				for match in matches:
					message = ("1/qwerty|n:" + user_id + "@" + randomCoordinates()).encode(self.encoding)
					if self.s.sendall(message) != None: raise Exception("Unable to send: " + message)
					else: print("Sent: ", message)

					# RECEIVING RESPONSE
					response = self.s.recv(self.buffer_size)
					print("Received: ", response)

			########################################

		finally:
			print("ERROR OCCURED")
			self.s.close()
			print("SOCKET CLOSED")


def test():
	distributor = ('127.0.0.1',7788)
	recv_buffer_length = 1024
	encoding = "utf-8"
	mobile = Mobile(distributor, recv_buffer_length, encoding)
	mobile.run()


if __name__ == "__main__":
	test()
